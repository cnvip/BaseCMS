#BaseCMS
Java后台系统，包含权限管理，内容管理等基础模块

主要技术

springmvc

springsecurity

hibernate

ehcache


演示地址：http://sharcar.duapp.com/

前台演示地址：http://sharcar.duapp.com/html/index_01/index.html

用户名：admin

密码：admin123

![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173827_585a6131_326874.jpeg "登录")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173904_367f6e75_326874.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173920_cde3d9b5_326874.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173929_d990a8e0_326874.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173938_38d0c27e_326874.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173948_2c978aab_326874.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/173957_67e128ac_326874.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0608/174008_3ab6f237_326874.png "在这里输入图片标题")