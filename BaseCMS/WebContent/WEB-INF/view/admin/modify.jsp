<%@ taglib prefix="myfn"
	uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form:form commandName="user"  method="post" id="form">
				<form:hidden path="password" />
				<form:hidden path="enabled" />
				<table class="table table-bordered">
					<c:if test="${message!=null }">
						<tr>
							<td colspan="2"><font color="red">操作失败：${message }</font></td>
						</tr>
					</c:if>
					<tr>
						<td>操作员账号:</td>
						<td><form:input path="username" readonly="true"
								class="form-control" /></td>
					</tr>
					<tr>
						<td>姓名:</td>
						<td><form:input path="realname" class="form-control" /></td>
					</tr>
					<tr>
						<td>部门:</td>
						<td><form:input path="department" class="form-control" /></td>
					</tr>
				</table>
				<p>
					权限:
					<span id="authError" style="color: red"></span>
				</p>

				<p>
					<c:forEach items="${roles}" var="role">
						<div style="float: left;">
							<input name="authorities" type="checkbox" value="${role.id}"
								<c:if test="${myfn:contains(user.authorities, role)}">checked='checked'</c:if> />
							${role.authority}
						</div>
					</c:forEach>
				</p>
				<div style="clear: both"></div>
				<hr />

				<input type="button" onclick="doSubmit()" value="保存"  class="btn btn-default">
			</form:form>
		</div>
	</div>
	<script type="text/javascript">
	function doSubmit(){
		var data = $("#form").serialize();
		$.ajax({
			url : "modify",
			data : data,
			type : 'post',
			contentType : 'application/x-www-form-urlencoded',
			encoding : 'UTF-8',
			cache : false,
			success : function(result) {
				if (result.success) {
					layer.alert('操作成功', {icon: 6},function(){
						var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	                    parent.layer.close(index);
					});
					parent.reload();
				} else {
					layer.alert('操作失败:'+result.obj);
				}
			},
			error:function(){
				layer.alert('系统异常');
			}
		});
	}
	</script>
</body>
</html>