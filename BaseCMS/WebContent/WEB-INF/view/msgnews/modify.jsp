<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<html:modify itemId="${entity.id }"
		titles="标题,作者,简介,描述,封面图片,匹配消息,外部链接"
		ids="title_required-input,author_required-input,brief_required-input,description_required-input,picpath_none-img,inputcode_required-input,fromurl_required-input"
		idValues="${entity.title },${entity.author },${entity.brief },${entity.description },${entity.picpath },${entity.inputcode }${entity.fromurl }">
		<%-- <div class="item">
			<span class="preTitle">状态:</span><font color="red"></font> 
			<html:select cssClass="form-control" collection="articleStatus" selectValue="${entity.status }" name="status" id="status">
			</html:select>
		</div> --%>
	</html:modify>
</body>
</html>