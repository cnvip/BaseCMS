<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<html:listPage ajaxUrl="msgnews"
		columns="title,author,brief,description,picpath,inputcode,fromurl,enable,readcount,favourcount,createDate,modifyDate"
		titles="标题,作者,简介,描述,封面图片,匹配消息,fromurl,是否可用,消息阅读数,消息点赞数,创建时间,修改时间"
		queryIds="title_like-input,inputcode_like-input,enable-select|00_正常/01_关闭,createDate_start|createDate_end-date,modifyDate_start|modifyDate_end-date"
		queryTitles="标题,匹配消息,状态,创建时间,修改时间">
	</html:listPage>
</body>
</html>