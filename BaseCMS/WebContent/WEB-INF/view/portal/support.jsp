<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>卡卡付-产品展示</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/support.css"/>"/>
</head>
<body>
	<div class="navbar-pay">		
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo"/></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" >主页</a></li>
					<li><a href="<c:url value="/portal/product"/>" >产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>" >成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>" >安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>" class="active">技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>" >关于我们</a></li>
				</ul> 
			</div>
		</div>
	</div>
	
	 <div class="sup">
		<img src="<c:url value="/images/portal/support/support.png"/>" />
		<div class="container">
		<c:forEach items="${products}" var="product">
			<div class="out row">
				<div class="col-md-2">
					<div>${product.productName}</div>
					<img src="<c:url value="${product.productImg}"/>" />
				</div>
				<div class="col-md-8">${product.description}</div>
				<div class="col-md-2"><button class="btn btn-default" type="button" onclick="javascript:window.open('${product.productUrl}','_self');;"><img src="<c:url value="/images/portal/support/sup-load.png"/>" />下载</button></div>
			</div>
			</c:forEach>
		</div>
	</div>
	
	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right Reserve 蜀ICP备16003612号</div>
	</div> 
</body>
</html>