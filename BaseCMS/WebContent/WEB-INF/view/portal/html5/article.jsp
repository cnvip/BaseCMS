<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${article.title }</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/aui/css/aui.css"/>'>
<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
<style type="text/css">
.title {
	padding-top: 15px;
	font-weight: bold;
	font-size: 18px;
}
</style>
</head>
<body>
	<div class="aui-content-padded">
		<div class="aui-text-center title">${article.title }</div>
	</div>
	<div class="aui-content-padded">
		<div class="aui-text-left aui-text-success">${article.content }</div>
	</div>
</body>
</html>