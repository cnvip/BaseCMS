<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>卡卡付-安全保障</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/security.css"/>"/>
</head>

<body>
	<div class="navbar-pay">		
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo"/></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" >主页</a></li>
					<li><a href="<c:url value="/portal/product"/>" >产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>" >成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>" class="active" >安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>" >技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>" >关于我们</a></li>
				</ul> 
			</div>
		</div>
	</div>
	
	<nav class="navbar navbar-default navbar-sec">
		<div class="container">
			<ul class="nav navbar-nav nav-sec">
				<li class="active"><a href="#">万变的网络欺诈</a><img src="<%=path %>/images/portal/cusp.png" /></li>
				<li><a href="#">安全保障支付无忧</a></li>				
			</ul>
		</div>
	</nav>
	
	<div class="sharp" id="sharp" >
		<div class="container">
			<div class="model">
				<div class="title">利用盗号和网络游戏交易进行诈骗</div>
				<div class="second">案例一：冒充即时通讯好友借钱</div>
				<div class="content">骗子使用黑客程序破解用户密码,然后张冠李戴冒名顶替向事主的聊天好友借钱,如果对方没有识别很容易上当。如果遇到类似情况一定要提高警惕,摸清对方的真实身份。需要您特别当心的是一些冒充熟人的网络视频诈骗，犯罪分子通过盗取图像的方式用“视频”与您聊天，您可千万别上当，遇上这种情况，最好先与朋友通过打电话等途径取得联系，防止被骗。</div>
				<div class="second">案例二：网络游戏装备及游戏币交易进行诈骗</div>
				<div class="content">常见的诈骗方式一是低价销售游戏装备，犯罪分子利用某款网络游戏，进行游戏币及装备的买卖，在骗取玩家信任后，让玩家通过线下银行汇款的方式，待得到钱款后即食言，不予交易；二是在游戏论坛上发表提供代练，待得到玩家提供的汇款及游戏账号后，代练一两天后连同账号一起侵吞；三是在交易账号时，虽提供了比较详细的资料，待玩家交易结束玩了几天后，账号就被盗了过去，造成经济损失。</div>
				<div class="second">案例三：交友诈骗</div>
				<div class="content">犯罪分子利用网站以交友的名义与事主初步建立感情，然后以缺钱等名义让事主为其汇款，最终失去联系。</div>
			</div>
			<div class="model">
				<div class="title">网络购物欺诈</div>
				<div class="second">案例一：多次汇款</div>
				<div class="content">骗子以未收到货款或提出要汇款到一定数目方能将以前款项退还等各种理由迫使事主多次汇款。</div>
				<div class="second">案例二：假链接、假网页</div>
				<div class="content">骗子为事主提供虚假链接或网页，交易往往显示不成功，让事主多次往里汇钱。</div>
				<div class="second">案例三：拒绝安全支付法</div>
				<div class="content">骗子以种种理由拒绝使用网站的第三方安全支付工具等。</div>
				<div class="second">案例四：收取订金骗钱法</div>
				<div class="content">骗子要求事主先付一定数额的订金或保证金，然后才发货。然后就会利用事主急于拿到货物的迫切心理以种种看似合理的理由，诱使事主追加订金。</div>
				<div class="second">案例五：约见汇款</div>
				<div class="content">网上购买二手车、火车票等诈骗的常见手法，骗子一方面约见事主在某地见面验车或给票，又要求事主的朋友一接到事主电话就马上汇款，骗子利用“来电任意显软件”冒充事主给其朋友打电话让其汇款。</div>
				<div class="second">案例六：以次充好</div>
				<div class="content">用假冒、劣质、低廉的山寨产品冒充名牌商品，事主收货后连呼上当，叫苦不堪。</div>
			</div>
			<div class="model">
				<div class="title">网上中奖诈骗</div>
				<div class="second">犯罪分子利用传播软件随意向邮箱用户、网络游戏用户、即时通讯用户等发布中奖提示信息，当事主按照指定的“电话”或“网页”进行咨询查证时，犯罪分子以中奖缴税等各种理由让事主一次次汇款，直到失去联系事主才发觉被骗。当您登陆聊天或打开邮箱时是否会收到一些来历不明的中奖提示，不管内容有多么逼真诱人，请您千万不能相信，更不要按照所谓的咨询电话或网页进行查证，否则您将一步步陷入骗局之中。</div>
			</div>
			<div class="model">
				<div class="title">网络“钓鱼”诈骗</div>
				<div class="second">“网络钓鱼”利用欺骗性的电子邮件和伪造的互联网站进行诈骗活动，获得受骗者财务信息进而窃取资金。作案手法有以下两种：</div>
				<div class="content">1、发送电子邮件，以虚假信息引诱用户中圈套。不法分子大量发送欺诈性电子邮件，邮件多以中奖、顾问、对账等内容引诱用户在邮件中填入金融账号和密码。
案例：网站声称把你写好的论文推荐给某些核心期刊，优先安排发表。按网站要求，先交50%的论文发表订金，在交了订金，把文稿寄过去后，就石沉大海、论文不能见刊不说，订金也白交了，哪里还能要回来！</div>
				<div class="content">2、不法分子通过设立假冒银行网站，当用户输入错误网址后，就会被引入这个假冒网站。一旦用户输入账号、密码，这些信息就有可能被犯罪分子窃取，账户里的存款可能被冒领。此外，犯罪分子通过发送含木马病毒邮件等方式，把病毒程序置入计算机内，一旦客户用这种“中毒”的计算机登录网上银行，其账号和密码也可能被不法分子所窃取，造成资金损失。</div>
				<div class="content">案例：嫌疑人在网上设立与知名杂志同一名称的虚假网站，给受害人发送其文章可以在网站录用的函，并给予其帐户，以让其向指定的帐户汇入版面费为由进行诈骗。
				</div>
			</div>
		</div>
	</div>
	
	<div class="pay" id="pay" style="display:none;" >
		<div class="modules">
			<div class="container">
				<img src="<%=path %>/images/portal/security/computer.png" />
				<div class="title">第一步 个人电脑环境安全</div>
				<div class="content">为个人电脑设置复杂的登录密码，使用第三方软件安全工具进行计算机的系统安全自动优化，及时更新杀毒 软件，升级操作系统补丁，打开病毒扫描功能，将IE浏览器的安全等级设为"中"或"高"，安装免费软件、共享软件时不默认安装，留意附带安装的插件内容</div>
			</div>
			<div class="bgground">
				<img src="<%=path %>/images/portal/arrow.png" class="arrow"/>
				<img src="<%=path %>/images/portal/security/secPay.png" class="srcPay"/>
				<div class="title">第二步 使用安全的支付工具</div>
				<div class="content" style="text-align:center;">手机或电脑安装数字证书，支付时使用短信验证码，使用动态口令完成支付，在电脑使用USB Key支付</div>
				<img src="<%=path %>/images/portal/arrow.png" class="arrow-bot"/>
			</div>
			<div class="bgground-white" style="margin-bottom: 50px;">				
				<img src="<%=path %>/images/portal/security/one.png" class="srcPay"/>
				<div class="title">第三步 个人安全意识</div>
				<div class="container">
					<div class="content" style="text-align:left;">妥善保管敏感信息：身份证信息、账户信息、银行卡信息、手机号等要妥善保管，不轻易提供给他人。不轻易在小网站或不知名的网站上预留以上信息。
	重视动态验证密码安全：短信验证码，动态口令等动态密码涉及到账户安全，资金安全，不提供给给任何他人，包括自称为工作人员或客服的人员。任何索取短信验证码的行为都属于诈骗行为。
	设置个性高强度的支付个人密码：单独设置，不与其他公共场合常用的密码相同。不要使用连续、重复、简单的数字祝贺或与本人生日，电话号码，身份证号等相关的数字信息。</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right Reserve 蜀ICP备16003612号</div>
	</div> 
<script>/*
	$(window).on('scroll',function(){
		var scrollTop = $(document).scrollTop();  //滚动条距离顶端的高度
		if(scrollTop>0){
			$('.navbar-pay .container').css({"padding-top":"8px"});
			$('.navbar-pay .nav-pay>li>a').css({"padding-bottom":"18px"});
		}else {
			$('.navbar-pay .container').css({"padding-top":"18px"});
			$('.navbar-pay .nav-pay>li>a').css({"padding-bottom":"26px"});
		}
	});*/
	
	$('.nav-sec li').on("click",function(){
		if($(this).hasClass("active")==false){
			$(this).addClass("active")
				   .siblings().removeClass("active");
			$(this).children().after('<img src="<%=path %>/images/portal/cusp.png" />');
			var len = $(this).siblings().children('img').length;   
			if(len>=1){
				$(this).siblings().children('img').remove();
			}
		}
		var val = $(this).children().text();
		if(val=="安全保障支付无忧"){
			$('#pay').show();
			$('#sharp').hide();
		}else {
			$('#pay').hide();
			$('#sharp').show();
		}
	}); 
	
	if(isFirefox=navigator.userAgent.indexOf("Firefox")>0){  
	    $('li img').css({"margin-bottom":"-8px"});
	}
</script>
</body>

</html>
