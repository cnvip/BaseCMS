<%@ page pageEncoding="UTF-8"%>
<!-- 提示框 -->
	<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true" >
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<div class="modal-title">提示</div>
		  </div>
		  <div class="modal-body">
		  	<div class="content" id="alert-content"></div>
			<div class="button">
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="alert-ok">确定</button>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	
	<!-- 确认框 -->
	<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true" >
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<div class="modal-title">提示</div>
		  </div>
		  <div class="modal-body">
		  	<div class="content" id="confirm-content"></div>
			<div class="button">
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="confirm-ok">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="confirm-cancle">取消</button>
			</div>
		  </div>
		</div>
	  </div>
	</div>