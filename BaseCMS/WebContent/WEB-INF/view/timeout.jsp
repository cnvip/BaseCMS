<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp"%>
<title>登录超时</title>
<script type="text/javascript">
  function toLogin(){
	  top.location="<%=request.getContextPath()%>/login?error=04";
	}
</script>
</head>
<body>
	<div class="form-container" id="search">

		<div style="align: center; color: red; margin: 20;">
			链接超时,或该账号在其他主机登录.请<a href="#" onclick="toLogin()">重新登录</a>
		</div>
	</div>
</body>
</html>
