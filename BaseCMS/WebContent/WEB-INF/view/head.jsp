<%@ page pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title>后台管理系统</title>

<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
 <![endif]-->

<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/bootstrap.min14ed.css?v=3.3.6"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/font-awesome.min93e3.css?v=4.4.0"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/animate.min.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/hplus/css/style.min862f.css?v=4.1.0"/>'>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/hplus/css/plugins/dataTables/dataTables.bootstrap.css"/>">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/form-table.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/modal.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/common.css"/>'>

<script type="text/javascript"
	src="<c:url value="/hplus/js/jquery.min.js?v=2.1.4"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/bootstrap.min.js?v=3.3.6"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/layer/layer.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/hplus.min.js?v=4.1.0"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/contabs.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/pace/pace.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/dataTables/jquery.dataTables.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/dataTables/dataTables.bootstrap.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/content.min.js?v=1.0.0"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/layer/layer.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/treeview/bootstrap-treeview.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/hplus/js/plugins/layer/laydate/laydate.js"/>"></script>
<!-- 富文本编辑器 -->
<script type="text/javascript"
	src="<c:url value="/js/ajaxfileupload.js"/>"></script>

<%-- <script type="text/javascript"
	src="<c:url value="/js/umeditor/umeditor.config.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/umeditor/umeditor.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/umeditor/lang/zh-cn/zh-cn.js"/>"></script>
<link
	href="<c:url value="/js/umeditor/themes/default/css/umeditor.css"/>"
	type="text/css" rel="stylesheet"> --%>

<script type="text/javascript"
	src="<c:url value="/js/ueditor/ueditor.config.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/ueditor/ueditor.all.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/ueditor/dialogs/135editor/135editor.js"/>"></script>
