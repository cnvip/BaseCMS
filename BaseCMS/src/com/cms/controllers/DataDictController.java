package com.cms.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.cms.entity.DataDict;
import com.cms.service.DataDictService;
import com.cms.support.Result;

/**
 * @ClassName: DataDictController
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年3月31日 下午2:51:58
 * 
 */
@Controller
public class DataDictController {
	@Autowired
	private DataDictService dataDictService;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DataDictController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {
	}

	@RequestMapping("/datadict")
	public String index() {
		return "datadict/index";
	}

	/**
	 * @Title: list
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @param dataDict
	 * @param iDisplayLength
	 * @param iDisplayStart
	 * @param sEcho
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/datadict/list")
	public Object list(Model model, DataDict dataDict, Integer iDisplayLength,
			Integer iDisplayStart, Integer sEcho) {
		try {
			Long totalCount = dataDictService.getCount(dataDict.getName(),
					dataDict.getValue(), dataDict.getDescription(),
					dataDict.getNameCN());
			List<DataDict> list = dataDictService.listDataDicts(
					dataDict.getName(), dataDict.getValue(),
					dataDict.getDescription(), dataDict.getNameCN(),
					iDisplayStart, iDisplayLength);
			JSONObject getObj = new JSONObject();
			getObj.put("sEcho", sEcho);// 不知道这个值有什么用,有知道的请告知一下
			getObj.put("iTotalRecords", totalCount);// 实际的行数
			getObj.put("iTotalDisplayRecords", totalCount);// 显示的行数,这个要和上面写的一样
			getObj.put("aaData", list);// 要以JSON格式返回
			return getObj;
		} catch (Exception e) {
			LOGGER.error(e.toString());
			return null;
		}
	}

	/**
	 * @Title: add
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @return String
	 * @throws
	 */
	@RequestMapping(value = "/datadict/add", method = RequestMethod.GET)
	public String add() {
		return "datadict/add";
	}

	/**
	 * @Title: add
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param datadict
	 * @param model
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/datadict/add", method = RequestMethod.POST)
	public Object add(@ModelAttribute("datadict") DataDict datadict, Model model) {
		try {
			if (null != datadict && datadict.getName() != null
					&& datadict.getValue() != null
					&& datadict.getDescription() != null) {
				if (dataDictService.queryByNameAndValue(datadict.getValue(),
						datadict.getName())) {
					return new Result(false, "字段值和字段名已存在");
				} else {
					dataDictService.add(datadict);
					return new Result();
				}
			} else {
				return new Result(false, "必填参数不能为空");
			}
		} catch (Exception e) {
			LOGGER.error("add datadict failed", e);
			return new Result(false, "系统异常");
		}
	}

	/**
	 * @Title: toModify
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @param id
	 * @return String
	 * @throws
	 */
	@RequestMapping(value = "datadict/modify", method = RequestMethod.GET)
	public String toModify(Model model, Integer id) {
		model.addAttribute("datadict", dataDictService.loadDataById(id));
		return "datadict/modify";
	}

	/**
	 * @Title: modify
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param dataDict
	 * @param model
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "datadict/modify", method = RequestMethod.POST)
	public Object modify(DataDict dataDict, Model model) {
		try {
			if (null != dataDict && dataDict.getName() != null
					&& dataDict.getValue() != null
					&& dataDict.getDescription() != null) {
				if (dataDictService.queryByNameAndValue(dataDict.getName(),
						dataDict.getValue())) {
					return new Result(false, "字段值和字段名已存在");
				} else {
					dataDictService.modify(dataDict);
					return new Result();
				}
			} else {
				return new Result(false, "必填参数不能为空");
			}
		} catch (Exception e) {
			LOGGER.error("modify datadict failed", e);
			return new Result(false, "系统异常");
		}
	}

	/**
	 * @Title: delete
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param id
	 * @param model
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/datadict/delete", method = RequestMethod.POST)
	public Object delete(Integer id, Model model) {
		try {
			Boolean b = dataDictService.delete(id);
			model.addAttribute("count", dataDictService.getCount());
			if (b) {
				return new Result();
			} else {
				return new Result(false, "删除失败");
			}
		} catch (Exception e) {
			LOGGER.error("delete datadict failed", e);
			return new Result(false, "系统异常");
		}
	}
}
