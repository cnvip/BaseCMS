package com.cms.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.entity.Bus;

/**
 * 
 * @author 会踢球的葡萄
 * @date 2015-12-29
 * 
 */

@Controller
@RequestMapping(value = "bus")
public class BusController extends BaseController<Bus>{

	@Override
	protected String getPrefix() {
		return "bus";
	}

	@Override
	protected Class<?> getClazz() {
		return Bus.class;
	}


}
