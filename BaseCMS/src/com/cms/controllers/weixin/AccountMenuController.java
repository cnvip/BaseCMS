package com.cms.controllers.weixin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.controllers.BaseController;
import com.cms.entity.weixin.AccountMenu;

/** 
 * @ClassName: AccountMenuController 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author zhangp 
 * @date 2016年5月13日 下午1:57:17 
 *  
 */
@Controller
@RequestMapping(value = "accountMenu")
public class AccountMenuController extends BaseController<AccountMenu> {

	@Override
	protected String getPrefix() {
		return "accountMenu";
	}

	@Override
	protected Class<?> getClazz() {
		return AccountMenu.class;
	}

}
