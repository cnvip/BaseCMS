package com.cms.controllers.weixin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.entity.Article;
import com.cms.entity.Catgory;
import com.cms.service.ArticleService;
import com.cms.service.CatgoryService;
import com.cms.support.StringEditor;

/**
 * @ClassName: Html5Controller
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年5月19日 下午2:51:02
 * 
 */
@Controller
public class Html5Controller {

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		// 转换日期表达式
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd hh:mm:ss");
		// 创建 CustomDateEditor 对象
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		// 注册为日期类型的自定义编辑器
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(String.class, new StringEditor());
	}

	@Autowired
	private ArticleService articleService;
	@Autowired
	private CatgoryService catgoryService;

	/**
	 * @Title: index
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @return String
	 * @throws
	 */
	@RequestMapping("portal/html5/{wxAccount}/index")
	public String index(Model model, @PathVariable String wxAccount) {
		List<Catgory> catgories = catgoryService.list(wxAccount, null, null,
				null, 0, 6);
		List<Article> articles = articleService.list(wxAccount, null, null,
				null, null, null, null, null, 0, 15);
		model.addAttribute("catgories", catgories);
		model.addAttribute("articles", articles);
		return "portal/html5/index";
	}

	/**
	 * @Title: article
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param model
	 * @param @param wxAccount
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping("portal/html5/{wxAccount}/article")
	public String article(Model model, @PathVariable String wxAccount,
			Integer id) {
		model.addAttribute("article", articleService.loadById(id));
		return "portal/html5/article";
	}

}
