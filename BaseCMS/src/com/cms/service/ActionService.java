package com.cms.service;

import java.util.List;
import java.util.Set;

import com.cms.entity.Action;
import com.cms.entity.Role;

public interface ActionService {
	public List<Action> listActions();
	
	public Action loadAction(Integer id);
	
	public Action getActionByRoles(Set<Role> roles);
	
	public Action getActionByAuthority(Integer authority);
	
	public Action getActionByUsername(String username);
	
	public void assignActions(Integer authority, Integer[] actionIds);
	
	public List<Action> listDateActions(String title, Integer from, Integer to);

	public Long getCount(String title);

	public void saveAction(Action action);

	public Action getAction(Integer id);
	
	public void updateAction(Action action);

	public boolean removeById(Integer id);

	public Action findActionById(int i);
	
	public Action getActionByUrl(String url);

}