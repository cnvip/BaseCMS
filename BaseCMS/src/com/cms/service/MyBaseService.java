package com.cms.service;

import java.util.List;

public interface MyBaseService {

	/** 列表查询.支持模糊查询，范围查询 */
	public <T> List<T> listByExample(T exampleEntity, Integer from,
			Integer length);
	/** 计数的实体. */
	public Long countByExample(Object exampleEntity);
	/** 根据id查询. */
	public Object get(Class<?> clazz, Long id);

	/** 根据参数查询. */
	public Object getByProperty(Class<?> clazz, String property, String value);

	/** 增加或更新实体. */
	public void saveOrUpdate(Object entity);

	/** 删除指定的实体. */
	public void delete(Object entity);

	

	/** 删除指定的实体. */
	public void deleteById(Class<?> clazz, Long id);

	/** 根据条件更新. */
	public void updateByX(String hql, Object... objects);
}
