package com.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_cms_user")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private String userName;
	@Column
	private String password;
	@Column
	private String niceName;
	@Column
	private String faceUrl;
	// 状态 ０：待提交，１：待审核，２：审核通过，3：审核失败，4已注销
	@Column
	private Integer status;
	@Column
	private Date createDate;

	@Column
	private String drivingLicense;// 驾驶证
	@Column
	private String roadLicense;// 行驶证
	@Column
	private String taxiLicense;// 营运证
	@Column
	private String carImg;// 车辆照片
	@Column
	private Integer carType;// 是否是打单车，0是，1不是

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getNiceName() {
		return niceName;
	}

	public void setNiceName(String niceName) {
		this.niceName = niceName;
	}

	public String getFaceUrl() {
		return faceUrl;
	}

	public void setFaceUrl(String faceUrl) {
		this.faceUrl = faceUrl;
	}


	@Transient
	public String getStatusName() {
		String statusName = "正常";
		if (status != null) {
			if (status == 0) {
				statusName = "正常";
			}
			if (status == 1) {
				statusName = "注销";
			}
		}
		return statusName;
	}

	/**
	 * @return the drivingLicense
	 */
	public String getDrivingLicense() {
		return drivingLicense;
	}

	/**
	 * @param drivingLicense
	 *            the drivingLicense to set
	 */
	public void setDrivingLicense(String drivingLicense) {
		this.drivingLicense = drivingLicense;
	}

	/**
	 * @return the taxiLicense
	 */
	public String getTaxiLicense() {
		return taxiLicense;
	}

	/**
	 * @param taxiLicense
	 *            the taxiLicense to set
	 */
	public void setTaxiLicense(String taxiLicense) {
		this.taxiLicense = taxiLicense;
	}

	/**
	 * @return the roadLicense
	 */
	public String getRoadLicense() {
		return roadLicense;
	}

	/**
	 * @param roadLicense
	 *            the roadLicense to set
	 */
	public void setRoadLicense(String roadLicense) {
		this.roadLicense = roadLicense;
	}

	/**
	 * @return the carImg
	 */
	public String getCarImg() {
		return carImg;
	}

	/**
	 * @param carImg
	 *            the carImg to set
	 */
	public void setCarImg(String carImg) {
		this.carImg = carImg;
	}

	/**
	 * @return the carType
	 */
	public Integer getCarType() {
		return carType;
	}

	/**
	 * @param carType
	 *            the carType to set
	 */
	public void setCarType(Integer carType) {
		this.carType = carType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserReqDto [userName=" + userName + ", password=" + password
				+ ", niceName=" + niceName + ", faceUrl=" + faceUrl
				+ ", status=" + status + ", createDate=" + createDate
				+ ", drivingLicense=" + drivingLicense + ", roadLicense="
				+ roadLicense + ", taxiLicense=" + taxiLicense + ", carImg="
				+ carImg + ", carType=" + carType + "]";
	}

}
