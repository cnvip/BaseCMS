package com.cms.entity.weixin;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.cms.common.hibernate.BaseEntity;

/**
 * @ClassName: MsgText
 * @Description: TODO(文本消息)
 * @author zhangp
 * @date 2016年5月9日 下午1:58:03
 * 
 */
@SuppressWarnings("serial")
@Entity(name = "t_weixin_msgtext")
public class MsgText extends BaseEntity {

	@Column
	private String wxAccount;// 微信账号
	@Column
	private String content;// 消息内容
	@Column
	private String msgtype;// 消息类型;
	@Column
	private String inputcode;// 关注者发送的消息
	@Column
	private String rule;// 规则，目前是 “相等”
	@Column
	private Integer enable;// 是否可用
	@Column
	private Integer readcount;// 消息阅读数
	@Column
	private Integer favourcount;// 消息点赞数

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getInputcode() {
		return inputcode;
	}

	public void setInputcode(String inputcode) {
		this.inputcode = inputcode;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public Integer getEnable() {
		return enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	public Integer getReadcount() {
		return readcount;
	}

	public void setReadcount(Integer readcount) {
		this.readcount = readcount;
	}

	public Integer getFavourcount() {
		return favourcount;
	}

	public void setFavourcount(Integer favourcount) {
		this.favourcount = favourcount;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getWxAccount() {
		return wxAccount;
	}

	public void setWxAccount(String wxAccount) {
		this.wxAccount = wxAccount;
	}

	@Override
	public String toString() {
		return "MsgText [wxAccount=" + wxAccount + ", content=" + content
				+ ", msgtype=" + msgtype + ", inputcode=" + inputcode
				+ ", rule=" + rule + ", enable=" + enable + ", readcount="
				+ readcount + ", favourcount=" + favourcount + "]";
	}

}